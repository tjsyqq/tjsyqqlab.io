---
layout:  post
title:  april_half
date: 2018-03-31
---
01《中邪》720P[豆瓣评分7.0](https://movie.douban.com/subject/26820833/&z)  
![](http://wx1.sinaimg.cn/mw690/006VddJIly1fpvsb9nilqj30jg0dadk2.jpg)  
![](http://wx3.sinaimg.cn/mw690/006VddJIly1fpvsb83u97j30jg21inn1.jpg)  
大学生丁鑫和刘梦为拍摄农村风俗纪录片进入一个村落，在目睹了充满诡异气息的 “还人”仪式后，两人跟随“高人”王婆夫  
妇来到一处偏远的山庄拍摄驱邪过程。然而 事情却向更加诡异的方向发展，驱邪四人组经历着人性的考验，生命岌岌可  
危，而所发 生的一切都被丁鑫和刘梦的摄影机默默记录着  
链接: [https://pan.baidu.com/s/1hekTte6B6XhCcPTH8Db38w](https://pan.baidu.com/s/1hekTte6B6XhCcPTH8Db38w&z) 密码: 64ni            [防和谐备份下载](http://tadown.com/fs/alu4ln6yn9ga44a7/&z)  

02《通勤营救》1080P[豆瓣评分6.8](https://movie.douban.com/subject/7056414/&z)  
![](http://wx3.sinaimg.cn/mw690/006VddJIly1fpvstkm76oj30jg0d445l.jpg)  
![](http://wx3.sinaimg.cn/mw690/006VddJIly1fpvstin8okj30jg1tx1kx.jpg)  
尼森每天坐通勤车上下班的商人，不知情地被卷入了一场犯罪阴谋中，不仅威胁着他的生命，也威胁着身边人生命。  
链接: [https://pan.baidu.com/s/1meYszOPelZyU2gAibIsTDw](https://pan.baidu.com/s/1meYszOPelZyU2gAibIsTDw&z) 密码: pc8f            [防和谐备份下载](http://tadown.com/fs/dl4u7lfneyanbge0/&z)  

03《泰坦》主演:萨姆·沃辛顿[豆瓣评分](https://movie.douban.com/subject/26649627/&z)  
![](http://wx1.sinaimg.cn/mw690/006VddJIly1fpvtj74j6uj30jg0db10y.jpg)   
![](http://wx1.sinaimg.cn/mw690/006VddJIly1fpvtj3ro8aj30jg1tz1kx.jpg)   
未来世界，人们正寻找新的家园。飞行员里克·詹森(沃辛顿饰)为人类潜在利益参加军事实验，然而他变成了一个能在土星  
中生存的怪人。可军事实验的目的并不美好。  
链接: [https://pan.baidu.com/s/1Lyzh4WfkRUhJVRzJGCNWkQ](https://pan.baidu.com/s/1Lyzh4WfkRUhJVRzJGCNWkQ&z) 密码: dixk            [防和谐备份下载](http://tadown.com/fs/2lu6ln6yn7g82e04/&z)  

04《大世界》1080P[豆瓣评分7.1](https://movie.douban.com/subject/26954003/&z)  
![](http://wx3.sinaimg.cn/mw690/006VddJIly1fpvtptd9h6j30jg0d641b.jpg)   
![](http://wx3.sinaimg.cn/large/006VddJIly1fpvtpqt9xxj30jg1d5dlg.jpg)   
南方小城，大雨将至。工地司机小张为了挽救和女友的爱情，抢劫了老板的巨款，引发连锁反应。几股势力纷纷对小张展  
开追逐。原本没有关系的他们，命运产生了交集。在一番阴差阳错、啼笑皆非的交手之后，他们迎来了意想不到的结局，  
命运和他们开了一个大大的玩笑。历时三年，导演刘健一人身兼多职，用独特的画风描绘了在一个发生在一天之内的黑色  
幽默故事，呈现出一幅独特的时代风景画和社会众生相。  
链接: [https://pan.baidu.com/s/1YWVDuFd3YlJzTZH7NbsI3Q](https://pan.baidu.com/s/1YWVDuFd3YlJzTZH7NbsI3Q&z) 密码: k29v            [防和谐备份下载](http://tadown.com/fs/9l0u4l1ndydnag26/&z)  

05《不戴胸罩的女孩》主演: 李丽珍 高清修复版 [豆瓣评分6.0](https://movie.douban.com/subject/3920055/&z)  
![](http://www.69img.com/u/20180331/11232977.jpg)   
![](http://www.69img.com/u/20180331/11231541.jpg)   
讲述一个不断追求完美爱情的少女寻找真爱的故事。  
链接: [https://pan.baidu.com/s/18GthHcxeGJVjHS6CJyQBhQ](https://pan.baidu.com/s/18GthHcxeGJVjHS6CJyQBhQ&z) 密码: 2iwq            [防和谐备份下载](http://tadown.com/fs/3lul2nyn1g557de5/&z)  

06《三国机密之潜龙在渊》1-12集主演: 马天宇 / 韩东君 / 万茜[豆瓣评分6.7](https://movie.douban.com/subject/26801742/&z)  
![](http://wx4.sinaimg.cn/mw690/006VddJIly1fpvummszvjj30jg0d7n5k.jpg)   
![](http://wx4.sinaimg.cn/large/006VddJIly1fpvumso253j30jg2321jt.jpg)   
讲述了曹操迎奉献帝于许都，挟天子以令诸之时，汉献帝刘协周旋列强之间，与同道携手复辟汉室的故事  
链接: [https://pan.baidu.com/s/1tBab3JYkkMAjnVyLgqH6mQ](https://pan.baidu.com/s/1tBab3JYkkMAjnVyLgqH6mQ&z) 密码: 65az            [防和谐备份下载](http://tadown.com/fs/4lul0nynegfcff68/&z)  

07《遇见你真好》主演: 白客 / 蓝盈莹[豆瓣评分4.8](https://movie.douban.com/subject/26967920/?from=showing&z)  
![](http://wx4.sinaimg.cn/mw690/006VddJIly1fpvvtu2llpj30jg0d744s.jpg)   
![](http://wx3.sinaimg.cn/large/006VddJIly1fpvw3jmuabj30jg1gfqny.jpg)   
在被称为“高四”的紫荆复读学校是一所爱情集中营，梦想成为作家的张文生和珊妮在互损中萌生情愫，好友陈奇误认为  
文生横刀夺爱，与“坏学生”青龙在小酒馆相遇并商议各自的复仇计划。不料陈奇带去的巨型烟花被青龙向酒馆发泄中引  
发火灾，带出人命。于是，这所学校里的一个又一个神秘搞笑的爱情故事浮出水面  
链接: [https://pan.baidu.com/s/1E9vBEfb9yXgdg9cIqmVC5g](https://pan.baidu.com/s/1E9vBEfb9yXgdg9cIqmVC5g&z) 密码: 4wjs            [防和谐备份下载](http://tadown.com/fs/9lu0ln6ynag3d141/&z)  

08《蜜桃成熟时1993》高清修复版 粤语中字[豆瓣评分7.0](https://movie.douban.com/subject/1302698/&z)  
![](http://www.69img.com/u/20180331/12492163.jpg)   
![](http://www.69img.com/u/20180331/12492158.jpg)   
富家女阿珍（李丽珍 饰）狂野不羁，自幼受父母溺爱的她深信自己的处世哲学，只想要追寻自由简单生活与浪漫炽热  
的爱情。 暑假期间，父母安排其前往英国参加夏令营， 她却想趁此机会计划一个属于自己的假期，暗地里将机票送给  
好友，自己却前往男友处准备与其过甜蜜的二人世界。岂料发现男友正与另一女子鬼混，她一气之下开始自己的孤独  
旅途，一路上遇到了各色人士，有好色的流氓、不羁的浪子、有趣的夫妻......到底珍能否找到心中所爱呢？  
链接: [https://pan.baidu.com/s/1xwKhqxWcw3EK3Ld_m4XDzg](https://pan.baidu.com/s/1xwKhqxWcw3EK3Ld_m4XDzg&z) 密码: haaw            [防和谐备份下载](http://tadown.com/fs/7lu2ln3yn7g50cd7/&z)  

09《情难自制》高清修复版 粤语中字 主演: 翁虹 [豆瓣评分4.7](https://movie.douban.com/subject/2332539/&z)  
![](http://www.69img.com/u/20180331/12564435.jpg)   
![](http://www.69img.com/u/20180331/1256388.jpg)   
酒吧女公关NIKE，一向追求刺激经历，常逼男友马交对自己性虐待，以求得到快感，但马交为人正常，对这种游戏并  
不欣赏，多次规劝不果，两人感情转淡。另一方面心理医生白玫瑰，每天要应酬各种变态客人，亦感到厌倦，某日NIK  
E到白医务所接受治疗，一时兴起两人决定掉转身份……  
链接: [https://pan.baidu.com/s/1DkM944OqyMVU_nKfv5qudQ](https://pan.baidu.com/s/1DkM944OqyMVU_nKfv5qudQ&z) 密码: 2yvz            [防和谐备份下载](http://tadown.com/fs/0l2u6l9nbyanfgb3/&z)  

10《聊斋艳谭3：灯草和尚》高清修复版 主演: 陈宝莲 [豆瓣评分5.8](https://movie.douban.com/subject/4227664/&z)  
![](http://www.69img.com/u/20180331/13140891.jpg)   
![](http://www.69img.com/u/20180331/13135369.jpg)   
由陈宝莲等主演。讲述了侠士朱仲已有未婚妻，但为一壁画美女倾心，他恳求灯草和尚助他入画的故事。  
链接: [https://pan.baidu.com/s/1aAj6NDJYvzfuLs9vIU-IoQ](https://pan.baidu.com/s/1aAj6NDJYvzfuLs9vIU-IoQ&z) 密码: 3xk5            [防和谐备份下载](http://tadown.com/fs/2lcuelbn3ydneg13/&z)  

11《一个谣言的女人》韩国大尺度汉语字幕  
![](http://www.69img.com/u/20180331/13285728.jpg)   
![](http://www.69img.com/u/20180331/1328477.jpg)   
女主是个女艺人，本指望着拍戏进入娱乐圈，结果被不良导演强奸，还拍下许多大尺度照片和视频，而幕后指使竟然  
是自己曾经的编剧老师，女主为了报复她，假装前去老师家中住一段时间，期间认识了老师的儿子，老师的儿子是个  
恋物癖，经常偷内衣和丝袜自慰，对女主也非常喜欢，希望得到女主的肉体，而女主也刻意勾引他，从而能利用他完  
成自己的复仇大计....  
链接: [https://pan.baidu.com/s/19p3qcaav3wTFir7ZUESPfQ](https://pan.baidu.com/s/19p3qcaav3wTFir7ZUESPfQ&z) 密码: e7i8            [防和谐备份下载](http://tadown.com/fs/alu6ln5yn7gc6ea1/&z)  

12《两个小姨子2》韩国大尺度汉语字幕  
![](http://www.69img.com/u/20180331/13324579.jpg)   
![](http://www.69img.com/u/20180331/13323857.jpg)   
男主的妻子意外去世，只能跟两个小姨子生活在一起，两个年轻的小姨子荷尔蒙旺盛，更是对姐夫健硕的身体产生了  
欲望，一个小姨子只能将情感埋在心中，常常在洗澡的时候或者在自己房间自慰，幻想着姐夫，但常被姐夫发现；而  
另一个小姨子则比较主动，不仅每晚上装作害怕黑暗跟着姐夫睡，更是经常吃饭的时候用手去撩拨男主的下体，姐夫  
面对两个风骚小姨子的勾引，该如何选择呢.  
链接: [https://pan.baidu.com/s/1H5Kr0qzapd5zAB72gbN26g](https://pan.baidu.com/s/1H5Kr0qzapd5zAB72gbN26g&z) 密码: i7kt            [防和谐备份下载](http://tadown.com/fs/6lul9nyn2gb15fd5/&z)  

13《女朋友》韩国大尺度汉语字幕  
![](http://www.69img.com/u/20180331/13363122.jpg)   
![](http://www.69img.com/u/20180331/13362351.jpg)   
男主楼下搬来了一对男女朋友，天天晚上啪啪啪，男主对楼下的女人非常迷恋，常常用相机偷窥，然后看着他们做爱  
而自己打飞机，男主的女朋友其实很想跟男主做爱，但是男主心里想的全是楼下的女人，终于有一天，楼下的男女分  
手了，男主便背着女朋友偷偷接近楼下的女人，楼下的女人没有了男人滋润，也十分饥渴，这两个奸夫淫妇一下子达  
成了共识，而男主的女朋友似乎也发现了其中的猫腻......  
链接: [https://pan.baidu.com/s/1GglMu1JZ3lhfS7x18sZR-w](https://pan.baidu.com/s/1GglMu1JZ3lhfS7x18sZR-w&z) 密码: 9cui            [防和谐备份下载](http://tadown.com/fs/9lbu9ldn6yfn5g56/&z)  

14《激情伴侣》激情大尺度女同电影  
![](http://www.69img.com/u/20180331/13432388.jpg)   
![](http://www.69img.com/u/20180331/13431525.jpg)   
女主角劳拉因为过去被虐待和一团糟的亲密接触经历，正在努力寻找一位新情人和正常的感受，16岁的伊娃成为她的  
希望之光。伊娃是一位才华横溢的钢琴家，但被母亲强加于她的生活压住而幻想破灭。两人发展了看似不太可能的关  
系，劳拉对伊娃异常迷恋。因为伊娃所处困境，劳拉说服她离家出走，而两人很快陷入了一场激烈的纠缠之中。操纵  
、克制、互相依赖掀起了波澜，最终两人动态关系破裂，只能坚守自己的阵地。  
链接: [https://pan.baidu.com/s/1fUdMap5lFiTw9mWpn9abVw](https://pan.baidu.com/s/1fUdMap5lFiTw9mWpn9abVw&z) 密码: pkyh            [防和谐备份下载](http://tadown.com/fs/0l0u0l2ney7n5gc3/&z)  

15《我邻居的妻子3》岛国大尺度-换妻游戏超劲爆  
![](http://www.69img.com/u/20180331/13483738.jpg)   
![](http://www.69img.com/u/20180331/13482841.jpg)   
一对夫妻结婚久了，感情变得枯燥乏味，邻居的夫妇提出换妻的建议，并告诉他们这样能大大提高生活质量，并能找到  
得到更刺激的性爱体验，这对夫妻半信半疑，而邻居却经受不住了，不断的劝说换妻，夫妻俩终于心动，决定试一试，  
经验丰富又饥渴难耐的邻居两夫妻，也得以机会展开了猛烈攻势，四人变态又淫荡的内心展露无遗...  
链接: [https://pan.baidu.com/s/1en05FX8f-veOl1Nn0eBd6Q](https://pan.baidu.com/s/1en05FX8f-veOl1Nn0eBd6Q&z) 密码: p74i            [防和谐备份下载](http://tadown.com/fs/blualnayn0ge0fd4/&z)  

16《念力》 釜山行导演最新科幻喜剧大片 [豆瓣评分5.9](https://movie.douban.com/subject/26864178/&z)   
![](http://wx3.sinaimg.cn/mw690/006VddJIly1fpvyhagynfj30jg0djagh.jpg)   
![](http://wx2.sinaimg.cn/large/006VddJIly1fpvyh8arwtj30jg1arx2s.jpg)   
偶然获得超能力的父亲在女儿遭遇危机即将失去一切时利用超能力施以援手的故事。柳承龙将在片中展现装模作样又温  
暖动人的演技。而由沈恩京饰演的女儿则有着超强的生活能力，在炸鸡店打工的她励志要成为炸鸡店老板，但却在一夜  
之间身陷危机。  
链接: [https://pan.baidu.com/s/1KReC74Naw37klPFnDFJcww](https://pan.baidu.com/s/1KReC74Naw37klPFnDFJcww&z) 密码: 8wx7            [防和谐备份下载](http://tadown.com/fs/7lbuel1nay3n7g43/&z)  

未完待续  
