---
layout:  post
title:  years good
date: 2018-02-21
---

01《芳华》4K&1080P[豆瓣评分7.9](https://movie.douban.com/subject/26862829/&z)  
![](http://wx4.sinaimg.cn/mw690/006VddJIly1fojjg8jbduj30jg0d7wkv.jpg)  
![](http://wx2.sinaimg.cn/mw690/006VddJIly1fojjg7jh0qj30jg150e3u.jpg)  
讲述了在充满理想和激情的军队文工团，一群正值芳华的青春少年，经历着成长中的爱情萌发与充斥着变数的人生命运。  
链接: [https://pan.baidu.com/s/1nwTrejf](https://pan.baidu.com/s/1nwTrejf&z) 密码: 1j8p       [防和谐备份下载](http://tadown.com/fs/2l6ufl9ncyan1g13/&z)  

02《嘉年华》[豆瓣评分8.3](https://movie.douban.com/subject/27019527/&z)  
![](http://wx1.sinaimg.cn/mw690/006VddJIly1fojkdho764j30jg0d8grg.jpg)   
![](http://wx4.sinaimg.cn/mw690/006VddJIly1fojkdh4ljbj30jg15vkgz.jpg)   
讲述了在一家旅店打工的小米，碰巧成为在旅店发生的一起案件的唯一知情者，为了保住自己的工作，决定保持沉默，  
一番挣扎与挫折之后，她终于醒悟，说出事情的真相。  
链接: [https://pan.baidu.com/s/1miW0F7U](https://pan.baidu.com/s/1miW0F7U&z) 密码: 4cqi         [防和谐备份下载](http://tadown.com/fs/7lul0nyn4g78ce42/&z)  

03《战狼2》[豆瓣评分7.3](https://movie.douban.com/subject/26363254/&z)  
![](http://wx2.sinaimg.cn/mw690/006VddJIly1fojjwdja9uj30jg0eq7d2.jpg)   
![](http://wx4.sinaimg.cn/mw690/006VddJIly1fojjw0diiwj30jg34jnpd.jpg)   
讲述了脱下军装的冷锋被卷入了一场非洲国家的叛乱，本来能够安全撤离的他无法忘记军人的职责，重回战场展开救援。  
链接: [https://pan.baidu.com/s/1dGJxYcp](https://pan.baidu.com/s/1dGJxYcp&z) 密码: q9kr        [防和谐备份下载](http://tadown.com/fs/9lu9lnfyn8gad057/&z)  

04《摔跤吧！爸爸》[豆瓣评分7.3](https://movie.douban.com/subject/26387939/&z)  
![](http://wx3.sinaimg.cn/mw690/006VddJIly1fojk2gvdk5j30jg0d9gp5.jpg)   
![](http://wx4.sinaimg.cn/mw690/006VddJIly1fojk2hebdij30jg0wyaef.jpg)   
根据印度摔跤手马哈维亚·辛格·珀尕的真实故事改编，讲述了曾经的摔跤冠军辛格培养两个女儿成为女子摔跤冠军，打破  
印度传统的励志故事。  
链接: [https://pan.baidu.com/s/1jJhuf9k](https://pan.baidu.com/s/1jJhuf9k&z) 密码: 35mj        [防和谐备份下载](http://tadown.com/fs/9lul5nyn2gaebb28/&z)  

05《看不见的客人》[豆瓣评分8.8](https://movie.douban.com/subject/26580232/&z)  
![](http://wx4.sinaimg.cn/mw690/006VddJIly1fojkaquc3hj30jg0dp0z1.jpg)   
![](http://wx4.sinaimg.cn/mw690/006VddJIly1fojkapyynuj30jg1l6nlf.jpg)   
讲述了企业家艾德里安在事业如日中天之时被卷入一桩谋杀案中，为洗脱罪名请来了金牌女律师弗吉尼亚为自己辩护。  
链接: [https://pan.baidu.com/s/1cNrCaa](https://pan.baidu.com/s/1cNrCaa&z) 密码: tid6        [防和谐备份下载](http://tadown.com/fs/blublnayn8gc9874/&z)  

06《请以你的名字呼唤我》[豆瓣评分8.9](https://movie.douban.com/subject/26799731/&z)  
![](http://wx4.sinaimg.cn/mw690/006VddJIly1fojmta0pmvj30jg0d7acd.jpg)   
![](http://wx3.sinaimg.cn/mw690/006VddJIly1fojmt8yx4xj30jg15zki5.jpg)   
讲述了24岁的美国博士生奥利弗在意大利结识了17岁的少年艾利欧，两人从而发展出一段暧昧的关系。  
链接: [https://pan.baidu.com/s/1pNkNUFD](https://pan.baidu.com/s/1pNkNUFD&z) 密码: 4buh        [防和谐备份下载](http://tadown.com/fs/4luldnyn2g83e9c5/&z)  

07《海边的曼彻斯特》[豆瓣评分8.6](https://movie.douban.com/subject/25980443/&z)   
![](http://wx4.sinaimg.cn/mw690/006VddJIly1fojn2x16aij30jg0d540d.jpg)   
![](http://wx4.sinaimg.cn/mw690/006VddJIly1fojn2w8ad9j30jg16ih88.jpg)   
讲述了李·钱德勒的哥哥因病去世之后，他作为监护人照顾侄子的故事。该片获得2016美国电影协会十佳电影。 2017年  
6月，获得预告片界奥斯卡最佳独立电影预告片奖项。  
链接: [https://pan.baidu.com/s/1oAj4cMe](https://pan.baidu.com/s/1oAj4cMe&z) 密码: 3kqr        [防和谐备份下载](http://tadown.com/fs/flulfnynegb2c842/&z)  

08《银翼杀手2049》[豆瓣评分8.3](https://movie.douban.com/subject/10512661/&z)  
![](http://wx1.sinaimg.cn/mw690/006VddJIly1fojnceqov5j30jg0dbait.jpg)   
![](http://wx3.sinaimg.cn/mw690/006VddJIly1fojncdfc17j30jg0wytb8.jpg)   
故事背景设定在《银翼杀手》30年后，讲述了在人类与复制人共生的2049年，两个种族之间的矛盾升级，新一代银翼杀手  
K寻找到已销声匿迹多年的前银翼杀手，并联手再次制止了人类与复制人的命运之战。  
链接: [https://pan.baidu.com/s/1i5ZoBOh](https://pan.baidu.com/s/1i5ZoBOh&z) 密码: 99ym        [防和谐备份下载](http://tadown.com/fs/alau2l1n1y0n2gc3/&z)  

09《至暗时刻》[豆瓣评分8.6](https://movie.douban.com/subject/26761416/&z)  
![](http://wx1.sinaimg.cn/mw690/006VddJIly1fojnibhh37j30jg0dcn0w.jpg)   
![](http://wx4.sinaimg.cn/mw690/006VddJIly1fojniajjn1j30jg1foh62.jpg)   
讲述了备受纳粹势力侵袭的二战时期，英国首相丘吉尔面临黎明前的黑暗做出了影响世界历史进程决定。  
链接: [https://pan.baidu.com/s/1pM56zJP](https://pan.baidu.com/s/1pM56zJP&z) 密码: r9ce        [防和谐备份下载](http://tadown.com/fs/9lualn7ynfga5867/&z)  

10《敦刻尔克》[豆瓣评分8.5](https://movie.douban.com/subject/26607693/&z)   
![](http://wx3.sinaimg.cn/mw690/006VddJIly1fojnwpp47kj30jg0e2jy7.jpg)   
![](http://wx4.sinaimg.cn/mw690/006VddJIly1fojnwp1k1bj30jg24wu0x.jpg)   
改编自二战历史事件《敦刻尔克大撤退》，当时40万英法联军被敌军包围在敦刻尔克的海滩上，面对敌军步步逼近的绝  
境，他们不得不为自己的命运背水一战。  
链接: [https://pan.baidu.com/s/1htQJzTy](https://pan.baidu.com/s/1htQJzTy&z) 密码: cce2        [防和谐备份下载](http://tadown.com/fs/6l0u0l5n1yen2gf9/&z)  
  